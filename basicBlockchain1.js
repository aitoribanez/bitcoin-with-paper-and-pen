// sample basic blockchain.

const SHA256 = require("crypto-js/sha256");

class Block {
    constructor(index, timestamp, data, previousHash = '') {
        this.index = index;
        this.previousHash = previousHash;
        this.timestamp = timestamp;
        this.data = data;
        this.hash = this.calculateHash();
    }

    calculateHash() {
        return SHA256(this.index + this.previousHash + this.timestamp + JSON.stringify(this.data)).toString(); 
    }
}

class Blockchain {
    constructor() {
        this.chain = [this.createGenesisBlock()];    
    }

    createGenesisBlock() {
        return new Block(0, Date.parse("2018-01-01"), "Genesis block", "0");
    }

    getLatestBlock() {
        return this.chain[this.chain.length - 1];
    }

    addBlock(newBlock) {
        newBlock.previousHash = this.getLatestBlock().hash;
        newBlock.hash = newBlock.calculateHash();
        this.chain.push(newBlock);
    }

    isChainValid() {
        for (let i = 1; i < this.chain.length; i++) {
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i - 1];

            if (currentBlock.hash !== currentBlock.calculateHash()) {
                return false;
            }

            if (currentBlock.previousHash !== previousBlock.calculateHash()) {
                return false;
            }

            if (JSON.stringify(this.chain[0]) !== JSON.stringify(this.createGenesisBlock())) {
                return false;
            }
        }

        return true;
    }
}

let basicBlockchain = new Blockchain();

//print initial blockchain (genesis block)
console.log("----- cadena de bloques inicial (genesis block) ----");
console.log(JSON.stringify(basicBlockchain, null, 4));


console.log("************");

// Add block's

console.log("----- Añadir bloques ----");
basicBlockchain.addBlock(new Block(1, Date.parse("2018-08-01"), { amount: 4 }));
basicBlockchain.addBlock(new Block(2, Date.parse("2018-09-01"), { amount: 8 }));
console.log(JSON.stringify(basicBlockchain, null, 4));


console.log("************");

//blockchain validation

console.log("----- Validacion Blockchain ----");
console.log(basicBlockchain.isChainValid() ? "Blockchain valida" : "Blockchain NO valida");

console.log("************");

// changing the blockchain (chanching block 2)
console.log("----- Alterar el segundo bloque de la Blockchain ----");
console.log("Hash block 2 antes del cambio: " + basicBlockchain.chain[1].hash);
console.log("Cambiando amount bloque 2 ...");
basicBlockchain.chain[1].data = { amount: 100 }; // keep old hash
console.log(JSON.stringify(basicBlockchain.chain[1], null, 4));

console.log("************"); 

console.log("----- Validacion Blockchain ----");
console.log(basicBlockchain.isChainValid() ? "Blockchain valida" : "Blockchain NO valida");

console.log("************");

console.log("Cambiando hash del bloque 2 ...");
basicBlockchain.chain[1].hash = basicBlockchain.chain[1].calculateHash();
console.log(JSON.stringify(basicBlockchain.chain[1], null, 4));
//console.log("Nuevo Hash block 2 despues del cambio: " + basicBlockchain.chain[1].calculateHash());

console.log("************");

console.log("----- Validacion Blockchain ----");
console.log(basicBlockchain.isChainValid() ? "Blockchain valida" : "Blockchain NO valida");

console.log("************");

/* console.log("----- la nueva Blockchain no valida, se tendran que cambiar todos los bloques siguientes ----");
console.log(JSON.stringify(basicBlockchain, null, 4));

console.log("************"); */


/* console.log("----- Restaurar amount bloque 2 y actualizar el hash ----");
basicBlockchain.chain[1].data = { amount: 4 }; 
basicBlockchain.chain[1].hash = basicBlockchain.chain[1].calculateHash();
console.log(JSON.stringify(basicBlockchain, null, 4));

console.log("************"); */

/* console.log("----- Validacion Blockchain ----");
console.log(basicBlockchain.isChainValid() ? "Blockchain valida" : "Blockchain NO valida");

console.log("************"); */


//check again the blockchain
//console.log("Blockchain valid? " + basicBlockchain.isChainValid());

// print the new invalid blockchain object.
//console.log(JSON.stringify(basicBlockchain, null, 4));