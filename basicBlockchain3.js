// Mining reward & and transaction data.

const SHA256 = require("crypto-js/sha256");

class Transaction {
    constructor(fromAddress, toAddress, amount) {
        this.fromAddress = fromAddress;
        this.toAddress = toAddress;
        this.amount = amount;
    }
}

class Block {
    constructor(index, timestamp, transactions, previousHash = '') {
        this.index = index;
        this.previousHash = previousHash;
        this.timestamp = timestamp;
        this.transactions = transactions;
        this.hash = this.calculateHash();
        this.nonce = 0;
    }

    calculateHash() {
        return SHA256(this.index + this.previousHash + this.timestamp + JSON.stringify(this.transactions) + this.nonce).toString();
    }

    mineBlock(difficulty) {
        while (this.hash.substring(0, difficulty) !== Array(difficulty + 1).join("0")) {
            this.nonce++;
            // console.log("nonce: " + this.nonce);
            this.hash = this.calculateHash();
            console.log("hash candidato: " + this.hash);
        }
        console.log("HASH BLOQUE MINADO: " + this.hash);
    }
}

class Blockchain {
    constructor() {
        this.chain = [this.createGenesisBlock()];
        this.difficulty = 2;
        this.pendingTransactions = []; //mempool
        this.miningReward = 50;
    }

    createGenesisBlock() {
        return new Block(0, Date.parse("2018-08-01"), [new Transaction(null, "miner-address", 50)], "0");
    }

    getLatestBlock() {
        return this.chain[this.chain.length - 1];
    }

    minePendingTransactions(miningRewardAddress) {
        this.pendingTransactions.push(new Transaction(null, miningRewardAddress, this.miningReward));
        
        let block = new Block(this.getLatestBlock().index + 1, Date.now(), this.pendingTransactions, this.getLatestBlock().hash);
        block.mineBlock(this.difficulty);

        console.log('Block successfully mined! HASH: ' + block.hash);
        this.chain.push(block);

        this.pendingTransactions = [];
    }

    createTransaction(transaction) {
        this.pendingTransactions.push(transaction);
    }

    getBalanceOfAddress(address) {
        let balance = 0;
        for (const block of this.chain) {
            for (const trans of block.transactions) {
                if (trans.fromAddress === address) {
                    balance -= trans.amount;
                }
                if (trans.toAddress === address) {
                    balance += trans.amount;
                }
            }
        }
        return balance;
    }

    isChainValid() {
        for (let i = 1; i < this.chain.length; i++){
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i - 1];

            // check if the data is not corrupted on the way
            if (currentBlock.hash !== currentBlock.calculateHash()) {
                return false;
            }

            // if the previousHas is the same on the currentBlocks previousHash
            // and previousBlock calculcted hash
            if (currentBlock.previousHash !== previousBlock.calculateHash()) {
                return false;
            }

            // if the first block is the genesisBlock to check that we are not valid chain
            if (JSON.stringify(this.chain[0]) !== JSON.stringify(this.createGenesisBlock())) {
                return false;
            }
        }

        return true;
    }
}

let basicBlockchain = new Blockchain();

basicBlockchain.createTransaction(new Transaction('miner-address', 'address2', 50));
basicBlockchain.createTransaction(new Transaction('address2', 'address3', 10));
basicBlockchain.createTransaction(new Transaction('address3', 'address4', 10));
basicBlockchain.createTransaction(new Transaction('address2', 'address4', 20));
basicBlockchain.createTransaction(new Transaction('address4', 'address1', 10));

console.log('\nBalance of MINER is', basicBlockchain.getBalanceOfAddress('miner-address'));

console.log('\n Before mining pending transactions: ' + JSON.stringify(basicBlockchain.pendingTransactions, null, 4));

console.log('\n Starting the mining process...');
basicBlockchain.minePendingTransactions('miner-address');

console.log('\nBalance of MINER is', basicBlockchain.getBalanceOfAddress('miner-address'));

console.log('\nBalance of address1 is', basicBlockchain.getBalanceOfAddress('address1'));
console.log('\nBalance of address2 is', basicBlockchain.getBalanceOfAddress('address2'));
console.log('\nBalance of address3 is', basicBlockchain.getBalanceOfAddress('address3'));
console.log('\nBalance of address4 is', basicBlockchain.getBalanceOfAddress('address4'));

console.log('\n After mining pending transactions: ' + JSON.stringify(basicBlockchain.pendingTransactions, null, 4));

console.log('\n Starting the process again...');
basicBlockchain.minePendingTransactions('miner-address');

console.log('\nBalance of miner is', basicBlockchain.getBalanceOfAddress('miner-address'));

console.log('\nBalance of address1 is', basicBlockchain.getBalanceOfAddress('address1'));
console.log('\nBalance of address2 is', basicBlockchain.getBalanceOfAddress('address2'));
console.log('\nBalance of address3 is', basicBlockchain.getBalanceOfAddress('address3'));
console.log('\nBalance of address4 is', basicBlockchain.getBalanceOfAddress('address4'));

console.log('\n Chain of blocks: ' + JSON.stringify(basicBlockchain.chain, null, 4));

/* FINAL RESULTS:

Chain with genesis block and 2 blocks

Balance of miner is 100

Balance of address1 is 10

Balance of address2 is 20

Balance of address3 is 0

Balance of address4 is 20

*/