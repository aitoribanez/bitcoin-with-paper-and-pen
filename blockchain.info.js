
const fetchUrl =require("fetch").fetchUrl;
const SHA256 = require("crypto-js/sha256");


console.log("<--Bitcoin blockchain data--");
fetchUrl('https://blockchain.info/q/latesthash?cors=true',function(error,meta,body){console.log("Hash of the latest block: " + body.toString())});
fetchUrl('https://blockchain.info/q/bcperblock?cors=true',function(error,meta,body){console.log("Current block reward in BTC: " + body.toString())});
fetchUrl('https://blockchain.info/q/getdifficulty?cors=true',function(error,meta,body){console.log("Current difficulty target as a decimal number: " + body.toString())});
fetchUrl('https://blockchain.info/q/totalbc?cors=true',function(error,meta,body){console.log("Total Bitcoins in circulation (delayed by up to 1 hour]): " + body.toString())});
fetchUrl('https://blockchain.info/q/hashestowin?cors=true',function(error,meta,body){console.log("Average number of hash attempts needed to solve a block: " + body.toString())});
//fetchUrl('https://blockchain.info/latestblock',function(error,meta,body){console.log(body.toString())});



/*
console.log("<--HASH--");
console.log(SHA256("xxx.").toString());
console.log(SHA256("xxx.").toString());
console.log("--HASH-->");
*/